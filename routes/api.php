<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['middleware' => 'auth:api'], function () {
    Route::prefix('auth')->name('auth.')->group(function () {
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    });

    Route::prefix('user')->name('user.')->group(function () {
        Route::get('', 'Auth\UserController@current')->name('current');
        Route::patch('profile', 'Shop\UserController@updateProfile')->name('profile');
        Route::patch('password', 'Shop\UserController@updatePassword')->name('password');
    });

    Route::prefix('topups')->name('topups.')->group(function () {
        Route::get('', 'Shop\TopupController@index')->name('index');
        Route::get('{id}/get', 'Shop\TopupController@get')->name('get');
        Route::post('create', 'Shop\TopupController@create')->name('create');
        Route::get('{id}/cancel', 'Shop\TopupController@cancel')->name('cancel');
    });

    Route::prefix('orders')->name('orders.')->group(function () {
        Route::get('', 'Shop\OrderController@index')->name('index');
        Route::get('{id}/get', 'Shop\OrderController@get')->name('get');
        Route::post('create', 'Shop\OrderController@create')->name('create');
    });

    Route::prefix('tickets')->name('tickets.')->group(function () {
        Route::get('', 'Shop\TicketController@index')->name('index');
        Route::get('categories', 'Shop\TicketController@categories')->name('categories.index');
        Route::post('create', 'Shop\TicketController@create')->name('create');
        Route::get('{id}/get', 'Shop\TicketController@get')->name('get');
        Route::post('{id}/reply', 'Shop\TicketController@reply')->name('reply');
    });

    Route::get('currencies', 'Shop\CurrencyController@index')->name('currencies.index');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::prefix('auth')->name('auth.')->group(function () {
        Route::post('login', 'Auth\LoginController@login')->name('login');
        Route::post('register', 'Auth\RegisterController@register')->name('register');

        //Route::post('password/jabber', 'Auth\ForgotPasswordController@sendResetLinkJabber');
        //Route::post('password/reset', 'Auth\ResetPasswordController@reset');

        //Route::post('jabber/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
        //Route::post('jabber/resend', 'Auth\VerificationController@resend');
    });
});

Route::get('articles', 'Shop\ArticleController@index')->name('articles.index');

Route::prefix('categories')->name('categories.')->group(function () {
    Route::get('', 'Shop\CategoryController@index')->name('index');
    Route::get('{id}/get', 'Shop\CategoryController@get')->name('get');
});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topups', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreignUuid('currency_id')
                ->references('id')
                ->on('currencies')
                ->onDelete('cascade');
            $table->uuid('charge_id');
            $table->decimal('fiat_amount', 10, 2)->unsigned();
            $table->decimal('crypto_amount', 12, 12)->unsigned();
            $table->string('address');
            $table->enum('status', ['cancelled', 'pending', 'paid', 'confirmed'])->default('pending');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topups');
    }
}

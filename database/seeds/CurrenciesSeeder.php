<?php

use Illuminate\Database\Seeder;

class CurrenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Currency::create([
            'name' => 'Bitcoin',
            'slug' => 'bitcoin',
            'code' => 'BTC',
        ]);

        \App\Currency::create([
            'name' => 'Ethereum',
            'slug' => 'ethereum',
            'code' => 'ETH',
        ]);

        \App\Currency::create([
            'name' => 'Litecoin',
            'slug' => 'litecoin',
            'code' => 'LTC',
        ]);
    }
}

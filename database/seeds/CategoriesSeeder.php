<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        \App\TicketCategory::create([
            'name' => 'Anderes',
            'order' => '2',
        ]);

        \App\TicketCategory::create([
            'name' => 'Replace',
            'is_replace_category' => true,
            'order' => '1',
        ]);

        \App\Article::create([
            'title' => 'Willkommen im Shop',
            'content' => $faker->text,
        ]);

        $one = \App\Category::create([
            'name' => 'Test Kategorie',
            'slug' => 'test-kategorie',
            'order' => '1',
        ]);

        $two = \App\Category::create([
            'name' => 'Test Kategorie #1',
            'slug' => 'test-kategorie-1',
            'order' => '2',
        ]);

        // Products
        $prod = \App\Product::create([
            'name' => 'Test Produkt',
            'slug' => 'test-produkt',
            'order' => '1',
            'short_desc' => '',
            'long_desc' => '',
            'price' => 100,
            'category_id' => $one->id,
        ]);

        \App\Product::create([
            'name' => 'Test Produkt #1',
            'slug' => 'test-produkt-1',
            'order' => '2',
            'short_desc' => '',
            'long_desc' => '',
            'price' => 150,
            'category_id' => $one->id,
        ]);

        \App\Product::create([
            'name' => 'Test Produkt',
            'slug' => 'test-produkt-2',
            'order' => '1',
            'short_desc' => '',
            'long_desc' => '',
            'price' => 150,
            'category_id' => $two->id,
        ]);

        \App\Product::create([
            'name' => 'Test Produkt #1',
            'slug' => 'test-produkt-2-1',
            'order' => '2',
            'short_desc' => '',
            'long_desc' => '',
            'price' => 100,
            'category_id' => $two->id,
        ]);

        for ($i=0; $i < 500; $i++) {
            \App\ProductItem::create([
                'product_id' => $prod->id,
                'content' => $faker->text,
            ]);
        }
    }
}

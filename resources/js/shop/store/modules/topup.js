import axios from "axios";
import * as types from "../mutation-types";

// state
export const state = {
  topups: null
};

// getters
export const getters = {
  topups: state => state.topups
};

// mutations
export const mutations = {
  [types.FETCH_TOPUPS](state, { topups }) {
    state.topups = topups;
  }
};

// actions
export const actions = {
  async fetchTopups({ commit }) {
    try {
      const { data } = await axios.get(route("shop.topups.index"));
      commit(types.FETCH_TOPUPS, { topups: data });
    } catch (e) {}
  }
};

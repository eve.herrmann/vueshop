import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  currencies: null
}

// getters
export const getters = {
  currencies: state => state.currencies,
}

// mutations
export const mutations = {
  [types.FETCH_CURRENCIES] (state, { currencies }) {
    state.currencies = currencies
  },
}

// actions
export const actions = {
  async fetchCurrencies ({ commit }) {
    try {
      const { data } = await axios.get(route('shop.currencies.index'))

      commit(types.FETCH_CURRENCIES, { currencies: data })
    } catch (e) {
    }
  },
}

import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  categories: null,
  category: null,
}

// getters
export const getters = {
    categories: state => state.categories,
    category: state => state.category,
}

// mutations
export const mutations = {
  [types.FETCH_CATEGORIES] (state, { categories }) {
    state.categories = categories
  },
  [types.FETCH_CATEGORY] (state, { category }) {
    state.category = category
  },
}

// actions
export const actions = {
  async fetchCategories ({ commit }) {
    try {
      const { data } = await axios.get(route('shop.categories.index'))

      commit(types.FETCH_CATEGORIES, { categories: data })
    } catch (e) {
    }
  },
  async fetchCategory ({ commit }, id) {
    try {
      const { data } = await axios.get(route('shop.categories.get', id))

      commit(types.FETCH_CATEGORY, { category: data })
    } catch (e) {
    }
  },
}

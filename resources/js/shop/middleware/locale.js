import store from '~/shop/store'
import { loadMessages } from '~/shop/plugins/i18n'

export default async (to, from, next) => {
  await loadMessages(store.getters['lang/locale'])

  next()
}

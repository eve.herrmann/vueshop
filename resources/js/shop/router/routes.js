function page(path) {
  return () =>
    import(/* webpackChunkName: '' */ `~/shop/pages/${path}`).then(
      m => m.default || m
    );
}

export default [
  { path: "/", redirect: { name: "home" }, name: "welcome" },
  { path: "/home", name: "home", component: page("home.vue") },

  { path: "/login", name: "login", component: page("auth/login.vue") },
  { path: "/register", name: "register", component: page("auth/register.vue") },

  {
    path: "/panel",
    component: page("panel/index.vue"),
    children: [
      { path: "", redirect: { name: "panel.profile" } },
      {
        path: "profile",
        name: "panel.profile",
        component: page("panel/profile.vue")
      },
      {
        path: "password",
        name: "panel.password",
        component: page("panel/password.vue")
      },
      {
        path: "topups",
        name: "panel.topups",
        component: page("panel/topups/index.vue")
      },
      {
        path: "topups/:id",
        name: "panel.topups.get",
        component: page("panel/topups/topup.vue"),
        props: true
      },
      {
        path: "orders",
        name: "panel.orders",
        component: page("panel/orders/index.vue")
      },
      {
        path: "orders/:id",
        name: "panel.orders.get",
        component: page("panel/orders/order.vue"),
        props: true
      },
      {
        path: "tickets",
        name: "panel.tickets",
        component: page("panel/tickets/index.vue")
      },
      {
        path: "tickets/create",
        name: "panel.tickets.create",
        component: page("panel/tickets/create.vue")
      },
      {
        path: "tickets/:id",
        name: "panel.tickets.get",
        component: page("panel/tickets/show.vue"),
        props: true
      }
    ]
  },
  {
    path: "/adminn",
    component: { template: `<router-view></router-view>` },
    children: [
      {
        path: "",
        component: page("admin/index.vue")
      },
      {
        path: "profile",
        name: "panel.profile",
        component: page("panel/profile.vue")
      }
    ]
  },
  {
    path: "/shop",
    component: page("shop/index.vue"),
    children: [
      {
        path: "/category/:id",
        name: "shop.category",
        component: page("shop/category.vue"),
        props: true
      }
    ]
  },
  { path: "*", component: page("errors/404.vue") }
];

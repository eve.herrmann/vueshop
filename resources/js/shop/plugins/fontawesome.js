import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import { } from '@fortawesome/free-regular-svg-icons'

import {
  faUser, faLock, faSignOutAlt, faCog, faPlus, faCheckCircle, faClock, faListOl, faLifeRing
} from '@fortawesome/free-solid-svg-icons'

import {
  faBitcoin, faBtc
} from '@fortawesome/free-brands-svg-icons'

library.add(
  faUser, faLock, faSignOutAlt, faCog, faBitcoin, faBtc, faPlus, faCheckCircle, faClock, faListOl, faLifeRing
)

Vue.component('fa', FontAwesomeIcon)

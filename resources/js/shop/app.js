import Vue from 'vue'
import store from '~/shop/store'
import router from '~/shop/router'
import i18n from '~/shop/plugins/i18n'
import App from '~/shop/components/App'
import axios from 'axios'

import '~/shop/plugins'
import '~/shop/components'

Vue.config.productionTip = false

window.route = require('./plugins/route');

var permissionMixin = {
  methods: {
    can: function (permissionName) {
      let hasAccess;
      axios.get(`/permission/${permissionName}`)
          .then(() => {
              hasAccess = true;
          })
          .catch(() => {
              hasAccess = false;
          });
      return hasAccess;
    }
  }
}

/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  permissionMixin,
  ...App
})

<?php

namespace App;

use TypiCMS\NestableTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use NestableTrait, \App\Http\Traits\UsesUuid;
    
    protected $fillable = [
        'name', 'slug', 'order', 'parent_id',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}

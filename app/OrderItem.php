<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use \App\Http\Traits\UsesUuid;

    protected $fillable = [
        'order_id', 'content', 'price', 'name',
    ];
}

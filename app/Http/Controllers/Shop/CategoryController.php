<?php

namespace App\Http\Controllers\Shop;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::orderBy('order')->get()->nest();

        return response()->json($categories);
    }

    public function get(Request $request, $id)
    {
        $category = Category::where('id', $id)->with(['products' => function ($q) {
            $q->orderBy('order');
        }])->first();

        return response()->json($category);
    }
}

<?php

namespace App\Http\Controllers\Shop;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $articles = Article::get();

        return response()->json($articles);
    }
}

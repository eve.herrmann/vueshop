<?php

namespace App\Http\Controllers\Shop;

use App\Modules\Shop\Ticket\Requests\CreateTicketRequest;
use App\Modules\Shop\Ticket\Requests\ReplyTicketRequest;
use App\Ticket;
use App\TicketCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class TicketController.
 */
class TicketController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $tickets = Ticket::where('user_id', $user->id)->latest()->paginate(env('RECORDS_PER_PAGE', 5));

        return response()->json($tickets);
    }

    /**
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function get(Request $request, string $id)
    {
        $user = $request->user();
        $ticket = Ticket::where('user_id', $user->id)->where('id', $id)->with('ticket_replies')->first();

        return response()->json($ticket);
    }

    /**
     * @param CreateTicketRequest $request
     *
     * @return JsonResponse
     */
    public function create(CreateTicketRequest $request)
    {
        $user = $request->user();
        /** @var Ticket $ticket */
        $ticket = Ticket::create([
            'user_id' => $user->id,
            'category_id' => $request->get('category_id'),
            'order_id' => $request->get('order_id'),
            'name' => $request->get('name'),
        ]);

        $ticket->ticket_replies()->create([
            'user_id' => $user->id,
            'content' => $request->get('content'),
        ]);

        return response()->json($ticket);
    }

    /**
     * @param ReplyTicketRequest $request
     * @param string             $id
     *
     * @return JsonResponse
     */
    public function reply(ReplyTicketRequest $request, string $id)
    {
        $user = $request->user();
        $ticket = Ticket::where('user_id', $user->id)->whereNull('closed_at')->where('id', $id)->first();
        $ticket->ticket_replies()->create([
            'user_id' => $user->id,
            'content' => $request->get('content'),
        ]);

        return response()->json($ticket);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function categories(Request $request)
    {
        $categories = TicketCategory::orderBy('order')->get();

        return response()->json($categories);
    }
}

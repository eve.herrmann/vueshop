<?php

namespace App\Http\Controllers\Shop;

use App\Order;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();
        $orders = Order::where('user_id', $user->id)->latest()->paginate(5);

        return response()->json($orders);
    }

    public function get(Request $request, $id)
    {
        $user = $request->user();
        $order = Order::where('user_id', $user->id)->where('id', $id)->with('order_items')->first();

        return response()->json($order);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|uuid',
            'product_amount' => 'required|numeric',
        ]);

        $product = Product::where('id', $request->product_id)->first();
        $stock = $product->stock();

        if ($stock < $request->product_amount) {
            return response()->json('error');
        }

        $order = Order::create([
            'user_id' => $request->user()->id,
            'product_id' => $request->product_id,
            'name' => $product->name,
            'amount' => $request->product_amount,
        ]);

        return response()->json($order);
    }
}

<?php

namespace App\Http\Controllers\Shop;

use App\Modules\Shop\Topup\Requests\CreateTopupRequest;
use App\Modules\Shop\Topup\Services\TopupService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TopupController extends Controller
{
    /**
     * @var TopupService
     */
    private $topupService;

    /**
     * TopupController constructor.
     *
     * @param TopupService $topupService
     */
    public function __construct(TopupService $topupService)
    {
        $this->topupService = $topupService;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $topupList = $this->topupService->getTopupList($user);

        return response()->json($topupList);
    }

    /**
     * @param Request $request
     * @param string     $id
     *
     * @return JsonResponse
     */
    public function get(Request $request, string $id)
    {
        $user = $request->user();
        $topup = $this->topupService->getActiveTopups($user, $id);

        return response()->json($topup);
    }

    /**
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function cancel(Request $request, string $id): JsonResponse
    {
        $user = $request->user();
        $topup = $this->topupService->cancelCharge($user, $id);

        return response()->json($topup);
    }

    /**
     * @param CreateTopupRequest $request
     *
     * @return JsonResponse
     */
    public function create(CreateTopupRequest $request): JsonResponse
    {
        $user = $request->user();
        $topup = $this->topupService->topupAccount($user, $request->validated());

        return response()->json($topup);
    }
}

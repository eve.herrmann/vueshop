<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Article;
use App\Order;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function articles(Request $request)
    {
        $articles = Article::get();

        return response()->json($articles);
    }

    public function categories(Request $request)
    {
        $categories = Category::orderBy('order')->get()->nest();

        return response()->json($categories);
    }

    public function category(Request $request, $id)
    {
        $category = Category::where('id', $id)->with(['products' => function ($q) {
            $q->orderBy('order');
        }])->first();

        return response()->json($category);
    }

    public function order(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|uuid',
            'product_amount' => 'required|numeric',
        ]);

        $product = Product::where('id', $request->product_id)->first();
        $stock = $product->stock();

        if ($stock < $request->product_amount) {
            return response()->json('error');
        }

        $order = Order::create([
            'user_id' => $request->user()->id,
            'product_id' => $request->product_id,
            'name' => $product->name,
            'amount' => $request->product_amount,
        ]);

        return response()->json($order);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketCategory extends Model
{
    use \App\Http\Traits\UsesUuid;
    
    protected $fillable = [
        'name', 'order', 'is_replace_category',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}

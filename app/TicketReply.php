<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketReply extends Model
{
    use \App\Http\Traits\UsesUuid;
    
    protected $fillable = [
        'user_id', 'ticket_id', 'content',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    public function getContentAttribute($value)
    {
        return nl2br($value);
    }
}

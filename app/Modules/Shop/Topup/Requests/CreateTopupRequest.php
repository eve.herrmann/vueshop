<?php

declare(strict_types=1);

namespace App\Modules\Shop\Topup\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTopupRequest extends FormRequest
{
    public function rules()
    {
        return [
            'fiat_amount' => 'required|numeric|between:5,25000',
            'currency' => 'required|string',
        ];
    }
}

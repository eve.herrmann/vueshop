<?php

declare(strict_types=1);

namespace App\Modules\Shop\Topup\Services;

use App\Currency;
use App\Modules\Shop\Topup\Repositories\TopupRepository;
use App\Topup;
use App\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Shakurov\Coinbase\Coinbase;

class TopupService
{
    /**
     * @var Coinbase
     */
    private $coinbase;

    /**
     * @var TopupRepository
     */
    private $topupRepository;

    /**
     * TopupService constructor.
     *
     * @param Coinbase        $coinbase
     * @param TopupRepository $topupRepository
     */
    public function __construct(
        Coinbase $coinbase,
        TopupRepository $topupRepository
    ) {
        $this->coinbase = $coinbase;
        $this->topupRepository = $topupRepository;
    }

    /**
     * @param User   $authorized
     * @param string $topupId
     *
     * @return Topup
     */
    public function cancelCharge(User $authorized, string $topupId): Topup
    {
        $topup = Topup::where('user_id', $authorized->id)
            ->where('status', 'pending')
            ->where('id', $topupId)
            ->firstOrFail();
        $this->coinbase->makeRequest('post', "charges/{$topup->charge_id}/cancel");
        $topup->status = 'cancelled';
        $topup->save();

        return $topup;
    }

    /**
     * @param User $authorized
     *
     * @return LengthAwarePaginator
     */
    public function getTopupList(User $authorized): LengthAwarePaginator
    {
        return $this->topupRepository->getTopupList($authorized->id);
    }

    /**
     * @param User   $user
     * @param string $topupId
     *
     * @return null|Topup
     */
    public function getActiveTopups(User $user, string $topupId): ?Topup
    {
        return $this->topupRepository->getActiveTopups($user, $topupId);
    }

    /**
     * @param User  $authorized
     * @param array $data
     *
     * @return Topup
     */
    public function topupAccount(User $authorized, array $data): Topup
    {
        $currency = Currency::where('code', strtoupper($data['currency']))->firstOrFail();
        $charge = $this->coinbase->createCharge([
            'name' => 'Name',
            'description' => 'Description',
            'local_price' => [
                'amount' => $data['fiat_amount'],
                'currency' => 'EUR',
            ],
            'metadata' => [
                'customer_id' => $authorized->id,
                'customer_name' => $authorized->name,
            ],
            'pricing_type' => 'fixed_price',
        ]);

        return Topup::create([
            'user_id' => $authorized->id,
            'currency_id' => $currency->id,
            'charge_id' => $charge['data']['id'],
            'fiat_amount' => $charge['data']['pricing']['local']['amount'],
            'crypto_amount' => $charge['data']['pricing'][$currency->slug]['amount'],
            'address' => $charge['data']['addresses'][$currency->slug],
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Modules\Shop\Topup\Repositories;

use App\Topup;
use App\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class TopupRepository
{
    /**
     * @param User   $user
     * @param string $topupId
     *
     * @return Topup|null
     */
    public function getActiveTopups(User $user, string $topupId): ?Topup
    {
        return Topup::where('user_id', $user->id)
            ->where('status', '!=', Topup::CANCELED_STATUS)
            ->where('id', $topupId)
            ->with(['currency'])
            ->first();
    }

    /**
     * @param string $userId
     *
     * @return LengthAwarePaginator
     */
    public function getTopupList(string $userId): LengthAwarePaginator
    {
        return Topup::query()
            ->where('user_id', $userId)
            ->where('status', '!=', 'cancelled')
            ->with('currency')
            ->latest()
            ->paginate(env('RECORDS_PER_PAGE', 5));
    }
}

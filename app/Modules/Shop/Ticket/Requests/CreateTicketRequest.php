<?php

declare(strict_types=1);

namespace App\Modules\Shop\Ticket\Requests;

use App\Modules\Shop\Ticket\Rules\IsOrderRequiredRule;
use App\TicketCategory;
use Illuminate\Foundation\Http\FormRequest;

class CreateTicketRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => 'required|uuid|exists:ticket_categories,id',
            'order_id' => [
                $this->isTicketCategoryRequired() ? 'required' : 'nullable',
                'uuid',
                'exists:orders,id',
            ],
            'name' => 'required|string',
            'content' => 'required|string|max:4000',
        ];
    }

    /**
     * @return bool
     */
    protected function isTicketCategoryRequired()
    {
        $categoryId = $this->get('category_id');
        if ($categoryId === null) {
            return false;
        }

        return (bool) TicketCategory::query()
            ->where('id', $categoryId)
            ->pluck('is_replace_category')
            ->first();
    }
}

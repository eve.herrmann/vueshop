<?php

declare(strict_types=1);

namespace App\Modules\Shop\Ticket\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReplyTicketRequest extends FormRequest
{
    public function rules()
    {
        return [
            'content' => 'required|string|max:4000',
        ];
    }
}

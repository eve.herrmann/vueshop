<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use \App\Http\Traits\UsesUuid;
    
    protected $fillable = [
        'title', 'content',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function getContentAttribute($value)
    {
        return nl2br($value);
    }
}

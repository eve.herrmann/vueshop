<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductItem extends Model
{
    use \App\Http\Traits\UsesUuid;

    protected $fillable = [
        'product_id', 'content',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}

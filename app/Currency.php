<?php

namespace App;

use App\Http\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use UsesUuid;

    protected $fillable = [
        'name', 'slug', 'code', 'enabled',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'id',
    ];
}

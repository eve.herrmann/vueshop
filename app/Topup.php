<?php

namespace App;

use App\Http\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class Topup extends Model
{
    use UsesUuid;
    public const CANCELED_STATUS = 'canceled';

    protected $fillable = [
        'user_id', 'currency_id', 'charge_id', 'address', 'fiat_amount', 'crypto_amount', 'status',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'charge_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}

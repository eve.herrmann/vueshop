<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use \App\Http\Traits\UsesUuid;

    protected $fillable = [
        'name', 'slug', 'order', 'category_id', 'short_desc', 'long_desc', 'price',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $appends = ['stock'];

    public function product_items()
    {
        return $this->hasMany(ProductItem::class);
    }

    public function getStockAttribute()
    {
        return $this->stock();
    }

    public function stock()
    {
        $pendingItems = Order::where('product_id', $this->id)->where('status', 'pending')->sum('amount');
        return (int) $this->product_items()->count() - $pendingItems;
    }
}

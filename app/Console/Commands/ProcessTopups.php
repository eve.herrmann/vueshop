<?php

namespace App\Console\Commands;

use App\Topup;
use Illuminate\Console\Command;

class ProcessTopups extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'topups:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processes topups';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $topups = Topup::where('status', 'pending')->get();
        foreach ($topups as $topup) {
            $charge = \Coinbase::getCharge($topup->charge_id);

            if (isset($charge['data']['timeline'][count($charge['data']['timeline']) - 1]['status']) &&
                \strcmp($charge['data']['timeline'][count($charge['data']['timeline']) - 1]['status'], 'PENDING') == 0) {
                $topup->status = 'paid';
                $topup->save();
            }
        }

        $topups = Topup::where('status', 'paid')->get();
        foreach ($topups as $topup) {
            $charge = \Coinbase::getCharge($topup->charge_id);
            var_dump($charge);

            if (isset($charge['data']['confirmed_at'])) {
                $topup->status = 'confirmed';
                
                $topup->user->balance = $topup->user->balance + intval(floatval($charge['data']['pricing']['local']['amount']) * 100);
                $topup->user->save();
                $topup->save();
            }
        }
    }
}

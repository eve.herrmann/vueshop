<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use \App\Http\Traits\UsesUuid;

    protected $fillable = [
        'user_id', 'category_id', 'order_id', 'name',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $appends = ['last_reply_by'];

    public function ticket_replies()
    {
        return $this->hasMany(TicketReply::class);
    }

    public function category()
    {
        return $this->belongsTo(TicketCategory::class);
    }

    public function getLastReplyByAttribute()
    {
        return $this->lastReplyBy();
    }

    public function lastReplyBy()
    {
        return $this->with('ticket_replies')->latest()->first()->ticket_replies[0]->user_id;
    }
}

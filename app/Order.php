<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use \App\Http\Traits\UsesUuid;

    protected $fillable = [
        'user_id', 'product_id', 'status', 'amount', 'name',
    ];

    protected $appends = ['total_price'];

    public function order_items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function getTotalPriceAttribute()
    {
        return $this->totalPrice();
    }

    public function totalPrice()
    {
        if ($this->order_items()->exists()) {
            return (int) $this->order_items()->sum('price');
        }

        return (int) Product::where('id', $this->product_id)->get('price')->first()->price * $this->amount;
    }

    public function process()
    {
        $itemIDsToDestroy = [];
        $orderItems = [];
        $productItems = ProductItem::where('product_id', $this->product_id)->take($this->amount)->get();
        $product = Product::where('id', $this->product_id)->first();
        foreach ($productItems as $item) {
            $orderItems[] = [
                'order_id' => $this->id,
                'content' => $item->content,
                'price' => $product->price,
                'name' => $product->name,
            ];
            array_push($itemIDsToDestroy, $item->id);
        }
        
        ProductItem::destroy($itemIDsToDestroy);
        $this->order_items()->createMany($orderItems);
        $this->status = 'done';
        $this->save();
    }
}
